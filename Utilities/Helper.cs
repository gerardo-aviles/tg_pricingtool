﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Utilities
{
public     class Helper
    {
        public string GetPOSToken(string developerAuthToken, string customerAuthToken, string absoluteUri, int developerAuthTokenId, int customerId) {
            using (var hmac = new HMACSHA256(Encoding.UTF8.GetBytes(developerAuthToken + customerAuthToken)))
            {
                return $"{developerAuthTokenId}:{customerId}:{Convert.ToBase64String(hmac.ComputeHash(Encoding.UTF8.GetBytes(absoluteUri)))}";
            }
        }
        public static Dictionary<string, object> CreateGetEventsParams(string eventName, string eventStartDateTime, string eventEndDateTime, string venueName)
        {
            int customer_id = int.Parse(ConfigurationManager.AppSettings["Cust_Token_Id"]);
            string customer_auth_token = ConfigurationManager.AppSettings["Cust_Token"];

            int developer_auth_token_id = int.Parse(ConfigurationManager.AppSettings["Dev_Token_Id"]);
            string developer_auth_token = ConfigurationManager.AppSettings["Dev_Token"];
            string pos_base_address = ConfigurationManager.AppSettings["Pos_Base_Address"]; 
            string requesturi = $"https://webservices.ticketnetwork.com/posapi/{Constants.EventEntityName}/{customer_id}";
            string accept_header = ConfigurationManager.AppSettings["Accept_Header"];
            HMACSHA256 hmc = new HMACSHA256(Encoding.UTF8.GetBytes(developer_auth_token + customer_auth_token));
            byte[] hmres = hmc.ComputeHash(Encoding.UTF8.GetBytes(requesturi));
            string hash = Convert.ToBase64String(hmres);

            string auth_header = developer_auth_token_id + ":" + customer_id + ":" + hash;

            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("cust_token_id", customer_id);
            parameters.Add("cust_token", customer_auth_token);
            parameters.Add("dev_token_id", developer_auth_token_id);
            parameters.Add("developer_auth_token", developer_auth_token);
            parameters.Add("entity_name",Constants.EventEntityName);
            parameters.Add("request_uri", requesturi);
            parameters.Add("event_name", eventName);
            parameters.Add("event_start_datetime", eventStartDateTime);
            parameters.Add("event_end_datetime", eventEndDateTime);
            parameters.Add("venue_name", venueName);
            parameters.Add("auth_header", auth_header);
            parameters.Add("accept_header", accept_header);
            parameters.Add("base_address", pos_base_address);
            
            return parameters;
        }
    }

 
}
