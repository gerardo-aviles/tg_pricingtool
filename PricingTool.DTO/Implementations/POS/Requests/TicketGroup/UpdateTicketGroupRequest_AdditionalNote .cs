﻿using PricingTool.DTO.Interfaces.POS.Requests.TicketGroup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PricingTool.DTO.Interfaces.POS.General;

namespace PricingTool.DTO.Implementations.POS.Requests.TicketGroup
{
    public class UpdateTicketGroupRequest_AdditionalNote : IUpdateTicketGroupRequest_AdditionalNote
    {
        public int NoteDescriptionID { get; set; }
        public IAdditionalNoteVariable[] Variables { get; set; }
    }
}
