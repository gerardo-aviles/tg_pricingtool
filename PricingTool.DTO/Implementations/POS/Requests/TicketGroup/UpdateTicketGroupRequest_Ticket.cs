﻿using PricingTool.DTO.Interfaces.POS.Requests.TicketGroup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PricingTool.DTO.Implementations.POS.Requests.TicketGroup
{
    public class UpdateTicketGroupRequest_Ticket : IUpdateTicketGroupRequest_Ticket
    {
        public bool OnHand { get; set; }
        public string OnHandDate { get; set; }
    }
}
