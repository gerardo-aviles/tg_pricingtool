﻿using PricingTool.DTO.Interfaces.POS.Requests.TicketGroup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PricingTool.DTO.Implementations.POS.Requests.TicketGroup
{
    public class UpdateTicketGroupRequest : IUpdateTicketGroupRequest
    {
        public int TicketGroupID { get; set; }
        public int UpdatedByCSRID { get; set; }
        public int SplitRuleID { get; set; }
        public int ShippingMethodSpecialID { get; set; }
        public int NearTermDisplayOptionID { get; set; }
        public string BroadcastChannelIDs { get; set; }
        public string InternalNotes { get; set; }
        public string ExternalNotes { get; set; }
        public IUpdateTicketGroupRequest_AdditionalNote[] AdditionalNotes { get; set; }
        public decimal FacePrice { get; set; }
        public decimal RetailPrice { get; set; }
        public decimal WholesalePrice { get; set; }
        public IUpdateTicketGroupRequest_Ticket[] Tickets { get; set; }
    }
}
