﻿using PricingTool.DTO.Interfaces.POS.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PricingTool.DTO.Implementations.POS.Responses
{
    public class POSResponse : IPOSResponse
    {
        public bool Success { get; set; }
        public string Message { get; set; }
    }
}
