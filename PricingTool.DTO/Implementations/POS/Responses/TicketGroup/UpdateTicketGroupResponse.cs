﻿using PricingTool.DTO.Interfaces.POS.Responses.TicketGroup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PricingTool.DTO.Implementations.POS.Responses.TicketGroup
{
    public class UpdateTicketGroupResponse : POSResponse, IUpdateTicketGroupResponse
    {
        public int TicketGroup { get; set; }
    }
}
