﻿using PricingTool.DTO.Interfaces.POS.General.TicketGroup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PricingTool.DTO.Interfaces.POS.General;

namespace PricingTool.DTO.Implementations.POS.General.TicketGroup
{
    public class AdditionalNote : IAdditionalNote
    {
        public int TicketGroupID { get; set; }
        public int CategoryTicketGroupID { get; set; }
        public int NoteDescriptionID { get; set; }
        public IAdditionalNoteVariable[] Variables { get; set; }
    }
}
