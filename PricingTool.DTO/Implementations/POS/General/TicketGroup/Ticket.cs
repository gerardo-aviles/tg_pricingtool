﻿using PricingTool.DTO.Interfaces.POS.General.TicketGroup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PricingTool.DTO.Implementations.POS.General.TicketGroup
{
    public class Ticket : ITicket
    {
        public int TicketID { get; set; }
        public int TicketGroupID { get; set; }
        public int PurchaseOrderID { get; set; }
        public int SeatNumber { get; set; }
        public int SeatOrder { get; set; }
        public decimal ActualSoldPrice { get; set; }
        public bool OnHand { get; set; }
        public string OnHandDate { get; set; }
        public int InvoiceID { get; set; }
        public bool Available { get; set; }
        public decimal Cost { get; set; }
    }
}
