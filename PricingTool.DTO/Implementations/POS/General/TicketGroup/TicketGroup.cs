﻿using PricingTool.DTO.Interfaces.POS.General.TicketGroup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PricingTool.DTO.Implementations.POS.General.TicketGroup
{
    public class TicketGroup : ITicketGroup
    {
        public int TicketGroupID { get; set; }
        public int ExchangeTicketGroupID { get; set; }
        public int UploadTicketGroupID { get; set; }
        public int EventID { get; set; }
        public int UpdatedByCSRID { get; set; }
        public int TicketGroupCodeID { get; set; }
        public int ExposureTypeID { get; set; }
        public int SeatingTypeID { get; set; }
        public int StockTypeID { get; set; }
        public int TicketGroupTypeID { get; set; }
        public int SplitRuleID { get; set; }
        public int ShippingMethodSpecialID { get; set; }
        public int NearTermDisplayOptionID { get; set; }
        public int BroadcastChannelIDs { get; set; }
        public string Section { get; set; }
        public string Row { get; set; }
        public int OfficeID { get; set; }
        public string ETicketsAttachedDate { get; set; }
        public bool InstantDownload { get; set; }
        public string InternalNotes { get; set; }
        public string ExternalNotes { get; set; }
        public IAdditionalNote[] AdditionalNotes { get; set; }
        public decimal FacePrice { get; set; }
        public decimal RetailPrice { get; set; }
        public decimal WholesalePrice { get; set; }
        public ITicket[] Tickets { get; set; }
    }
}
