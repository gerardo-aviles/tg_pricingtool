﻿using PricingTool.DTO.Interfaces.POS.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PricingTool.DTO.Implementations.POS.General
{
    public class AdditionalNoteVariable : IAdditionalNoteVariable
    {
        public string Variable { get; set; }
        public int Value { get; set; }
    }
}
