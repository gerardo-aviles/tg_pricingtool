﻿using PricingTool.DTO.Interfaces.POS;
using System;
using System.Collections.Generic;

namespace PricingTool.DTO.APIReturnTypes
{
    public class GetEventsResponse : IEventGetOperation
    {
        public IEnumerable<IEvent> EventList { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public int TotalEventCount { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public bool Success { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public string Message { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
    }
}
