﻿using PricingTool.DTO.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PricingTool.DTO.Managers
{
    public abstract class ManagerBase
    {
        protected IDataClient dataClient;

        public ManagerBase(IDataClient client) {
            dataClient = client;
        }
    }
}
