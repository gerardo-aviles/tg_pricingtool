﻿using Newtonsoft.Json;
using PricingTool.DTO.APIReturnTypes;
using PricingTool.DTO.Interfaces;
using PricingTool.DTO.Interfaces.POS;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace PricingTool.DTO.Managers.POS
{
    public class EventManager : ManagerBase, IEventManager
    {
        public EventManager(IDataClient client) : base(client) { }

        
        public async Task<IEventGetOperation> EventSearch(Dictionary<string, object> parameters)
        {
            string jsonString = string.Empty;
            
            jsonString= await dataClient.GetAsync(parameters);

            return ParseJson(jsonString);
        }

        private IEventGetOperation ParseJson(string json) {
            
            JsonSerializer serializer = new JsonSerializer();

            MemoryStream ms = new MemoryStream(UTF8Encoding.UTF8.GetBytes(json.ToCharArray()));

            StreamReader sr = new StreamReader(ms);
            
            JsonTextReader jr = new JsonTextReader(sr);
            
            return serializer.Deserialize<GetEventsResponse>(jr);
        }
    }
}
