﻿using PricingTool.DTO.Interfaces.POS;

namespace PricingTool.DTO.Types.POS
{
    public class TicketsInfoV5 : ITicketsInfo
    {
        public int Quantity { get; set; }
        public double TotalWholesagePrice { get; set; }

    }
}
