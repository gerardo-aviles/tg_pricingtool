﻿using PricingTool.DTO.Interfaces.POS;
using System;

namespace PricingTool.DTO.Types.POS
{
    public class EventV6 : IEvent
    {
        public int EventID { get; set; }
        public int PosEventID { get; set; }
        public string EventName { get; set; }
        public DateTime EventDateAndTime { get; set; }
        public int VenueID { get; set; }
        public string VenueName { get; set; }
        public string VenueCity { get; set; }
        public string VenueState { get; set; }
        public string VenuePostalCode { get; set; }

        public TicketsInfoV5 AvailableTickets { get; set; }
        public TicketsInfoV5 SoldTickets { get; set; }
        public TicketsInfoV5 OnHoldTickets { get; set; }
        public TicketsInfoV5 LockedTickets { get; set; }

    }


}
