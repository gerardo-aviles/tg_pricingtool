﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PricingTool.DTO.Interfaces
{
    public interface IDataClient
    {
        Task<string> GetAsync(Dictionary<string, object> parameters);
    }
}
