﻿

namespace PricingTool.DTO.Interfaces.POS.General
{
    public interface IAdditionalNoteVariable
    {
        string Variable { get; set; }
        int Value { get; set; }
    }
}
