﻿

namespace PricingTool.DTO.Interfaces.POS.General.TicketGroup
{
    public interface ITicket
    {
        int TicketID { get; set; }
        int TicketGroupID { get; set; }
        int PurchaseOrderID { get; set; }
        int SeatNumber { get; set; }
        int SeatOrder { get; set; }
        decimal ActualSoldPrice { get; set; }
        bool OnHand { get; set; }
        string OnHandDate { get; set; }
        int InvoiceID { get; set; }
        bool Available { get; set; }
        decimal Cost { get; set; }
    }
}
