﻿

namespace PricingTool.DTO.Interfaces.POS.General.TicketGroup
{
    public interface IAdditionalNote
    {
        int TicketGroupID { get; set; }
        int CategoryTicketGroupID { get; set; }
        int NoteDescriptionID { get; set; }
        IAdditionalNoteVariable[] Variables { get; set; }
    }
}
