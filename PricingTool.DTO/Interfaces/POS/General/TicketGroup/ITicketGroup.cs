﻿

namespace PricingTool.DTO.Interfaces.POS.General.TicketGroup
{
    public interface ITicketGroup
    {
        int TicketGroupID { get; set; }
        int ExchangeTicketGroupID { get; set; }
        int UploadTicketGroupID { get; set; }
        int EventID { get; set; }
        int UpdatedByCSRID { get; set; }
        int TicketGroupCodeID { get; set; }
        int ExposureTypeID { get; set; }
        int SeatingTypeID { get; set; }
        int StockTypeID { get; set; }
        int TicketGroupTypeID { get; set; }
        int SplitRuleID { get; set; }
        int ShippingMethodSpecialID { get; set; }
        int NearTermDisplayOptionID { get; set; }
        int BroadcastChannelIDs { get; set; }
        string Section { get; set; }
        string Row { get; set; }
        int OfficeID { get; set; }
        string ETicketsAttachedDate { get; set; }
        bool InstantDownload { get; set; }
        string InternalNotes { get; set; }
        string ExternalNotes { get; set; }
        IAdditionalNote[] AdditionalNotes { get; set; }
        decimal FacePrice { get; set; }
        decimal RetailPrice { get; set; }
        decimal WholesalePrice { get; set; }
        ITicket[] Tickets { get; set; }
    }
}
