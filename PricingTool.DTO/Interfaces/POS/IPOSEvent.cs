﻿using PricingTool.DTO.Interfaces.POS;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PricingTool.DTO.Interfaces.POS
{
    public interface IEventManager
    {

        Task<IEventGetOperation> EventSearch(Dictionary<string, object> parameters);
    
    }
}
