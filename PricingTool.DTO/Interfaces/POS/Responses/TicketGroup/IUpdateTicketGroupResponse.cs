﻿

namespace PricingTool.DTO.Interfaces.POS.Responses.TicketGroup
{
    public interface IUpdateTicketGroupResponse : IPOSResponse
    {
        int TicketGroup { get; set; }
    }
}
