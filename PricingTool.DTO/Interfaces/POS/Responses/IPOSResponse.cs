﻿

namespace PricingTool.DTO.Interfaces.POS.Responses
{
    public interface IPOSResponse
    {
        bool Success { get; set; }
        string Message { get; set; }
    }
}
