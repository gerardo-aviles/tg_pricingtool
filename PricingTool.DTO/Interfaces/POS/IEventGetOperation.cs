﻿using System.Collections.Generic;

namespace PricingTool.DTO.Interfaces.POS
{
    public interface IEventGetOperation
    {
        IEnumerable<IEvent> EventList { get; set; }
        int TotalEventCount { get; set; }
        bool Success { get; set; }
        string Message { get; set; }
    }
}
