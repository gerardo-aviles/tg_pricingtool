﻿namespace PricingTool.DTO.Interfaces.POS
{
    public interface ITicketsInfo
    {
        int Quantity { get; set; }
        double TotalWholesagePrice { get; set; }
    }
}