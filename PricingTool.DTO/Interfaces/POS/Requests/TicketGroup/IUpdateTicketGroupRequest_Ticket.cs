﻿

namespace PricingTool.DTO.Interfaces.POS.Requests.TicketGroup
{
    public interface IUpdateTicketGroupRequest_Ticket
    {
        bool OnHand { get; set; }
        string OnHandDate { get; set; }
    }
}
