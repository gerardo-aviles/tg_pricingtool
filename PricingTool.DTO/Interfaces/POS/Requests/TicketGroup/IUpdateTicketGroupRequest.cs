﻿

namespace PricingTool.DTO.Interfaces.POS.Requests.TicketGroup
{
    public interface IUpdateTicketGroupRequest
    {
        int TicketGroupID { get; set; }
        int UpdatedByCSRID { get; set; }
        int SplitRuleID { get; set; }
        int ShippingMethodSpecialID { get; set; }
        int NearTermDisplayOptionID { get; set; }
        string BroadcastChannelIDs { get; set; }
        string InternalNotes { get; set; }
        string ExternalNotes { get; set; }
        IUpdateTicketGroupRequest_AdditionalNote[] AdditionalNotes { get; set; }
        decimal FacePrice { get; set; }
        decimal RetailPrice { get; set; }
        decimal WholesalePrice { get; set; }
        IUpdateTicketGroupRequest_Ticket[] Tickets { get; set; }
    }
}
