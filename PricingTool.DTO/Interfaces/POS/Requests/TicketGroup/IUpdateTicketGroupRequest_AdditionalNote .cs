﻿

using PricingTool.DTO.Interfaces.POS.General;

namespace PricingTool.DTO.Interfaces.POS.Requests.TicketGroup
{
    public interface IUpdateTicketGroupRequest_AdditionalNote
    {
        int NoteDescriptionID { get; set; }
        IAdditionalNoteVariable[] Variables { get; set; }
    }
}
