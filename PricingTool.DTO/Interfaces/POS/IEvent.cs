﻿using PricingTool.DTO.Types.POS;
using System;

namespace PricingTool.DTO.Interfaces.POS
{
    public interface IEvent
    {
        TicketsInfoV5 AvailableTickets { get; set; }
        DateTime EventDateAndTime { get; set; }
        int EventID { get; set; }
        string EventName { get; set; }
        TicketsInfoV5 LockedTickets { get; set; }
        TicketsInfoV5 OnHoldTickets { get; set; }
        int PosEventID { get; set; }
        TicketsInfoV5 SoldTickets { get; set; }
        string VenueCity { get; set; }
        int VenueID { get; set; }
        string VenueName { get; set; }
        string VenuePostalCode { get; set; }
        string VenueState { get; set; }
    }
}