﻿using PricingTool.DTO.Interfaces;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace PricingTool.Comm
{
    public class TGWebClient : IDataClient
    {
        HttpClient client = new HttpClient();

        public TGWebClient()
        {

        }

        public async Task<string> GetAsync(Dictionary<string, object> parameters)
        {
            try
            {
                client.BaseAddress = new Uri(parameters["base_address"].ToString());
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(parameters["accept_header"].ToString()));
                client.DefaultRequestHeaders.Add("X-API-Authorization", parameters["auth_header"].ToString());

                string returnValue = string.Empty;
                HttpResponseMessage response = await client.GetAsync(parameters["request_uri"].ToString());
                if (response.IsSuccessStatusCode)
                {
                    returnValue = (string)await response.Content.ReadAsStringAsync();
                }
                return returnValue;
            }
            catch (Exception e) { }
            return null;
        }
    }
}
