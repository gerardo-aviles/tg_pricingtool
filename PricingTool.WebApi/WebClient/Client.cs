﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;

namespace PricingTool.WebApi.WebClient
{
    public class Client
    {
        HttpClient client = new HttpClient();

        public Client(string uriAddress, string acceptHeader = "application/json") {
            client.BaseAddress = new Uri(uriAddress);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue(acceptHeader));
        }

        async Task<string> GetAsync(string path)
        {
            string returnValue = string.Empty;
            HttpResponseMessage response = await client.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                returnValue = (string)await response.Content.ReadAsStringAsync();
            }
            return returnValue;
        }
    }
}