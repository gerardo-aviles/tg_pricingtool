﻿using PricingTool.Comm;
using PricingTool.DTO.Managers.POS;
using System.Web.Http;


namespace PricingTool.WebApi.Controllers
{

    public class POSEventController : ApiController
    {
        
        // GET: api/POSEvent/5
        public IHttpActionResult Get(string eventName=null, string eventStartDateTime=null, string eventEndDateTime=null, string venueName=null )
        {
            EventManager mgr = new EventManager(new TGWebClient());

            var returnValue = mgr.EventSearch(Utilities.Helper.CreateGetEventsParams(eventName, eventStartDateTime, eventEndDateTime, venueName));

            return Ok(Json(returnValue));
        }

        // POST: api/POSEvent
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/POSEvent/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/POSEvent/5
        public void Delete(int id)
        {
        }
    }
}
